package com.atlassian.plugins.osgi.test.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @since version
 */
public class BundleResultRepresentation
{
    @JsonProperty
    private final String bundleSymbolicName;
    @JsonProperty private final TestResultDetailRepresentation testResultDetail;

    @JsonCreator
    public BundleResultRepresentation(@JsonProperty("bundleSymbolicName") String bundleSymbolicName, @JsonProperty("testResultDetail") TestResultDetailRepresentation testResultDetail)
    {
        this.bundleSymbolicName = bundleSymbolicName;
        this.testResultDetail = testResultDetail;
    }

    public String getBundleSymbolicName()
    {
        return bundleSymbolicName;
    }

    public TestResultDetailRepresentation getTestResultDetail()
    {
        return testResultDetail;
    }
}
