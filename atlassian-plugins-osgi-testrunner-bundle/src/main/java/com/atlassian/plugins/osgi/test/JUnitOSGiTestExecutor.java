package com.atlassian.plugins.osgi.test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.internal.runners.statements.RunAfters;
import org.junit.internal.runners.statements.RunBefores;
import org.junit.rules.RunRules;
import org.junit.rules.TestRule;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

import java.util.List;
import java.util.Set;


/**
 * @since version
 */
public class JUnitOSGiTestExecutor extends BlockJUnit4ClassRunner
{
    private Object testInstance;
    private JunitOSGiRunListener osgiListener;
    
    public JUnitOSGiTestExecutor(Class<?> klass,Object testInstance) throws InitializationError
    {
        super(klass);
        this.testInstance = testInstance;
    }

    @Override
    public void run(final RunNotifier notifier) {
        this.osgiListener = new JunitOSGiRunListener(notifier);
        notifier.addListener(osgiListener);
        
        super.run(notifier);
    }
    
    @Override
    protected Statement methodBlock(FrameworkMethod method) {
        
        Statement statement= methodInvoker(method, testInstance);
        statement= possiblyExpectingExceptions(method, testInstance, statement);
        statement= withPotentialTimeout(method, testInstance, statement);
        statement= withBefores(method, testInstance, statement);
        statement= withAfters(method, testInstance, statement);
        statement= withRules(method, testInstance, statement);
        return statement;
    }

    @Override
    protected void validateConstructor(List<Throwable> errors) {
        //Dont validate the constructor
    }

    @Override
    protected Statement withBeforeClasses(Statement statement)
    {
        List<FrameworkMethod> befores= getTestClass().getAnnotatedMethods(BeforeClass.class);
        return befores.isEmpty() ? statement : new RunBefores(statement, befores, testInstance);
    }

    @Override
    protected Statement withAfterClasses(Statement statement)
    {
        List<FrameworkMethod> afters= getTestClass().getAnnotatedMethods(AfterClass.class);
        return afters.isEmpty() ? statement : new RunAfters(statement, afters, testInstance);
    }

    private Statement withRules(FrameworkMethod method, Object target,
                                Statement statement) {
        Statement result= statement;
        result= withMethodRules(method, target, result);
        result= withTestRules(method, target, result);
        return result;
    }

    private Statement withTestRules(FrameworkMethod method, Object target,
                                    Statement statement) {
        List<TestRule> testRules= getTestRules(target);
        return testRules.isEmpty() ? statement :
                new RunRules(statement, testRules, describeChild(method));
    }
    
    @SuppressWarnings("deprecation")
    private Statement withMethodRules(FrameworkMethod method, Object target,
                                      Statement result) {
        List<TestRule> testRules= getTestRules(target);
        for (org.junit.rules.MethodRule each : getMethodRules(target))
            if (! testRules.contains(each))
                result= each.apply(result, method, target);
        return result;
    }

    @SuppressWarnings("deprecation")
    private List<org.junit.rules.MethodRule> getMethodRules(Object target) {
        return rules(target);
    }

    public Set<String> getPassedMethodNames()
    {
        return osgiListener.getPassedMethodNames();
    }

    public Set<String> getFailedMethodNames()
    {
        return osgiListener.getFailedMethodNames();
    }

    public Set<String> getIgnoredMethodNames()
    {
        return osgiListener.getIgnoredMethodNames();
    }
}
