package com.atlassian.plugins.osgi.test.spring;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.osgi.test.OsgiTestClassLoader;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.osgi.framework.BundleContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class SpringBeans {
    @Autowired
    private BundleContext context;

    @Bean
    public UserManager userManager() {
        return importOsgiService(UserManager.class);
    }

    @Bean
    public LoginUriProvider loginUriProvider() {
        return importOsgiService(LoginUriProvider.class);
    }

    @Bean
    public WebSudoManager salWebSudoManager() {
        return importOsgiService(WebSudoManager.class);
    }
    @Bean
    public ApplicationProperties applicationProperties() {
        return importOsgiService(ApplicationProperties.class);
    }
    @Bean
    public TemplateRenderer renderer() {
        return importOsgiService(TemplateRenderer.class);
    }

    @Bean
    public I18nResolver i18nResolver() {
        return importOsgiService(I18nResolver.class);
    }

    @Bean
    public PluginAccessor pluginAccessor() {
        return importOsgiService(PluginAccessor.class);
    }

    @Bean
    public OsgiTestClassLoader osgiTestLoader() {
        return new OsgiTestClassLoader(context);
    }
}
