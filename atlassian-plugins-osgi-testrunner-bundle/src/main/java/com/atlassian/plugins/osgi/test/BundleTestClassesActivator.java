package com.atlassian.plugins.osgi.test;

import org.osgi.framework.*;

/**
 * @since version
 */
public class BundleTestClassesActivator implements BundleActivator
{
    private BundleListener bundleListener;

    @Override
    public void start(BundleContext bundleContext) throws Exception
    {

        /**
             * Add a listener for future bundle registrations
             */
        bundleListener = new SynchronousBundleListener()
        {

            public void bundleChanged(BundleEvent event)
            {
                switch (event.getType())
                {
                    case BundleEvent.RESOLVED:
                    {
                        addBundle(event.getBundle());
                        break;
                    }

                    case BundleEvent.STARTING:
                    {
                        addBundle(event.getBundle());
                        break;
                    }

                    case BundleEvent.STARTED:
                    {
                        addBundle(event.getBundle());
                        break;
                    }

                    case BundleEvent.UNINSTALLED:
                    {
                        removeBundle(event.getBundle());
                        break;
                    }

                    case BundleEvent.STOPPED:
                    {
                        removeBundle(event.getBundle());
                        break;
                    }
                }
            }
        };

        bundleContext.addBundleListener(bundleListener);

        /**
         * process any bundles that are already active/resolved.
         */
        for (final Bundle bundle : bundleContext.getBundles())
        {
            if (bundle.getState() == Bundle.ACTIVE || bundle.getState() == Bundle.RESOLVED)
            {
                addBundle(bundle);
            }
        }
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception
    {
        for (final Bundle bundle : bundleContext.getBundles())
        {
            if (bundle.getState() == Bundle.ACTIVE || bundle.getState() == Bundle.RESOLVED)
            {
                removeBundle(bundle);
            }
        }

        if (bundleListener != null)
        {
            bundleContext.removeBundleListener(bundleListener);
        }
    }

    private void addBundle(Bundle bundle)
    {
        if (bundle.getSymbolicName().endsWith("-tests"))
        {
            BundleTestClassesManager.instance().registerBundle(bundle);
        }
    }

    private void removeBundle(Bundle bundle)
    {
        if (bundle.getSymbolicName().endsWith("-tests"))
        {
            BundleTestClassesManager.instance().unRegisterBundle(bundle);
        }
    }
}
