package com.atlassian.plugins.osgi.test.util;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.sal.api.ApplicationProperties;

/**
 * @since version
 */
public class ContextPathContextProvider implements ContextProvider
{
    private final ApplicationProperties applicationProperties;

    public ContextPathContextProvider(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
    }

    @Override
    public void init(Map<String, String> stringStringMap) throws PluginParseException
    {
       
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> stringObjectMap)
    {
        final Map<String, Object> ctx = new HashMap<String, Object>();
        ctx.put("contextPath",applicationProperties.getBaseUrl());
        
        return ctx;
    }
}
