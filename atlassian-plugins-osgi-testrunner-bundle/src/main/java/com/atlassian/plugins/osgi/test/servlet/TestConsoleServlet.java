package com.atlassian.plugins.osgi.test.servlet;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import com.atlassian.plugins.osgi.test.BundleTestClassesManager;
import com.atlassian.plugins.osgi.test.OsgiTestClassLoader;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.Bundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TestConsoleServlet extends HttpServlet
{
    static final String JIRA_SERAPH_SECURITY_ORIGINAL_URL = "os_security_originalurl";
    static final String CONF_SERAPH_SECURITY_ORIGINAL_URL = "seraph_originalurl";
    public static final String CONSOLE_TEMPLATE = "/templates/it-test-console.vm";
    public static final String BUNDLE_TEMPLATE = "/templates/it-test-bundles.vm";
    public static final String ERROR_TEMPLATE = "/templates/it-test-error.vm";
    public static final String BUNDLE_PARAM = "bundle";
    private final TemplateRenderer renderer;
    private final ApplicationProperties applicationProperties;
    private final PluginAccessor pluginAccessor;
    private final OsgiTestClassLoader testLoader;
    private final LoginUriProvider loginUriProvider;
    private final WebSudoManager webSudoManager;
    private final UserManager userManager;

    public TestConsoleServlet(TemplateRenderer renderer, ApplicationProperties applicationProperties, PluginAccessor pluginAccessor, OsgiTestClassLoader testLoader, LoginUriProvider loginUriProvider, WebSudoManager webSudoManager, UserManager userManager)
    {
        this.renderer = renderer;
        this.applicationProperties = applicationProperties;
        this.pluginAccessor = pluginAccessor;
        this.testLoader = testLoader;
        this.loginUriProvider = loginUriProvider; 
        this.webSudoManager = webSudoManager; 
        this.userManager = userManager;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            webSudoManager.willExecuteWebSudoRequest(request);
            if(!isAdmin())
            {
                redirectToLogin(request,response);
                return;
            }
        }
        catch (WebSudoSessionException wse)
        {
            webSudoManager.enforceWebSudoProtection(request, response);
            return;
        }
        
        response.setContentType("text/html;charset=utf-8");
        final Map<String, Object> context = new HashMap<String, Object>();
        context.put("contextPath",applicationProperties.getBaseUrl());
        context.put("productName",applicationProperties.getDisplayName().toLowerCase());
        
        
        BundleTestClassesManager manager = BundleTestClassesManager.instance();
        String bundle = request.getParameter(BUNDLE_PARAM);
        
        Set<Bundle> osgiBundles = new HashSet<>(testLoader.findAllBundles());
        Set<Bundle> testBundles = new HashSet<>(osgiBundles);
        testBundles.addAll(manager.getAllBundles());
        
        if(null == bundle)
        {
            if(testBundles.size() > 1)
            {
                List<BundleNameAndPlugin> bundleNamesAndPlugins = new ArrayList<>();
                
                for(Bundle testBundle : testBundles)
                {
                    Plugin plugin = null;
                    String pluginKey = OsgiHeaderUtil.getPluginKey(testBundle);
                    if (StringUtils.isNotBlank(pluginKey))
                    {
                        plugin = pluginAccessor.getPlugin(pluginKey);
                    }
                    
                    if(null != plugin)
                    {
                        bundleNamesAndPlugins.add(new BundleNameAndPlugin(testBundle.getSymbolicName(),plugin));
                    }
                }
                
                context.put("bundleNamesAndPlugins",bundleNamesAndPlugins);
                renderer.render(BUNDLE_TEMPLATE,context,response.getWriter());
                return;
            }
            else if(!testBundles.isEmpty())
            {
                bundle = testBundles.iterator().next().getSymbolicName();
            }
            
        }
        
        if(null != bundle && (hasBundleKey(bundle,testBundles)))
        {
            Bundle ourBundle = getBundleForKey(bundle,testBundles);
            String pluginKey = OsgiHeaderUtil.getPluginKey(ourBundle);
            Plugin plugin = null;
            if (StringUtils.isNotBlank(pluginKey))
            {
                plugin = pluginAccessor.getPlugin(pluginKey);
            }
            
            if(null != plugin)
            {
                context.put("testPlugin",plugin);
            }

            if(testBundles.size() > 1)
            {
                context.put("hasMultipleTestPlugins",true);
            }
            else
            {
                context.put("hasMultipleTestPlugins",false);
            }
            
            context.put("bundleSymbolicName",bundle);
            renderer.render(CONSOLE_TEMPLATE,context,response.getWriter());
        }
        else
        {
            context.put("bundleSymbolicName",bundle);
            renderer.render(ERROR_TEMPLATE,context,response.getWriter());
        }
    }

    private boolean hasBundleKey(String bundleKey, Set<Bundle> bundles)
    {
        boolean hasKey = false;
        
        for(Bundle bundle : bundles)
        {
            if(bundle.getSymbolicName().equals(bundleKey))
            {
                hasKey = true;
                break;
            }
        }
        
        return hasKey;
    }

    private Bundle getBundleForKey(String bundleKey, Set<Bundle> bundles)
    {
        Bundle foundBundle = null;

        for(Bundle bundle : bundles)
        {
            if(bundle.getSymbolicName().equals(bundleKey))
            {
                foundBundle = bundle;
                break;
            }
        }

        return foundBundle;
    }

    protected boolean isAdmin()
    {
        if (userManager.getRemoteUsername() == null)
        {
            return false;
        }
        return userManager.isSystemAdmin(userManager.getRemoteUsername()) || userManager.isAdmin(userManager.getRemoteUsername());
    }

    protected void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        final URI uri = getUri(request);
        
        String uriString = uri.toASCIIString();
        request.getSession().setAttribute(JIRA_SERAPH_SECURITY_ORIGINAL_URL, uriString);
        request.getSession().setAttribute(CONF_SERAPH_SECURITY_ORIGINAL_URL, uriString);

        response.sendRedirect(loginUriProvider.getLoginUri(uri).toASCIIString());
    }

    protected URI getUri(HttpServletRequest request)
    {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null)
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
    
    public class BundleNameAndPlugin {
        private final String bundleSymbolicName;
        private final String bundleDivId;
        private final Plugin plugin;

        public BundleNameAndPlugin(String bundleSymbolicName, Plugin plugin)
        {
            this.bundleSymbolicName = bundleSymbolicName;
            this.bundleDivId = bundleSymbolicName.replaceAll("\\.","_");
            this.plugin = plugin;
        }

        public String getBundleSymbolicName()
        {
            return bundleSymbolicName;
        }

        public String getBundleDivId()
        {
            return bundleDivId;
        }

        public Plugin getPlugin()
        {
            return plugin;
        }
    }
}
