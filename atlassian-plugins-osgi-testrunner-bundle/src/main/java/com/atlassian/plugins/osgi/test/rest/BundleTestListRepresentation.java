package com.atlassian.plugins.osgi.test.rest;

import com.atlassian.plugins.osgi.test.util.TestClassUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @since version
 */
public class BundleTestListRepresentation
{
    @JsonProperty
    private final String bundleSymbolicName;
    @JsonProperty private final Map<TestClassUtils.TestType,List<TestDetailRepresentation>> testMap;

    @JsonCreator
    public BundleTestListRepresentation(@JsonProperty("bundleSymbolicName") String bundleSymbolicName)
    {
        this.bundleSymbolicName = bundleSymbolicName;
        this.testMap = new HashMap<>(3);
        testMap.put(TestClassUtils.TestType.Wired,new ArrayList<>());
        testMap.put(TestClassUtils.TestType.Unit,new ArrayList<>());
        testMap.put(TestClassUtils.TestType.IT,new ArrayList<>());
    }

    public String getBundleSymbolicName()
    {
        return bundleSymbolicName;
    }

    public Map<TestClassUtils.TestType, List<TestDetailRepresentation>> getTestMap()
    {
        return testMap;
    }
    
    public List<TestDetailRepresentation> getUnitTests()
    {
        return testMap.get(TestClassUtils.TestType.Unit);
    }

    public List<TestDetailRepresentation> getITTests()
    {
        return testMap.get(TestClassUtils.TestType.IT);
    }

    public List<TestDetailRepresentation> getWiredTests()
    {
        return testMap.get(TestClassUtils.TestType.Wired);
    }
    
}
