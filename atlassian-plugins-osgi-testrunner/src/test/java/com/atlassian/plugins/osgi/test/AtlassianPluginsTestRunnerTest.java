package com.atlassian.plugins.osgi.test;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.Description;

import static com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner.BASE_URL;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.rules.ExpectedException.none;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AtlassianPluginsTestRunnerTest
{
    @Rule
    public ExpectedException expectedException = none();

    @Test
    public void resourceUrlGenerationShouldFailIfBaseUrlNotSet()
    {
        // Set up
        final Description mockDescription = mock(Description.class);
        System.clearProperty(BASE_URL);
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage(is("Missing base URL 'null'; is the 'baseurl' system property set?"));

        // Invoke
        AtlassianPluginsTestRunner.getResourceUrl(mockDescription);
    }

    @Test
    public void resourceUrlGenerationShouldSucceedIfBaseUrlIsSet()
    {
        // Set up
        final Description mockDescription = mock(Description.class);
        final String testClassName = "MyTest";
        when(mockDescription.getClassName()).thenReturn(testClassName);
        System.setProperty(BASE_URL, "http://www.example.com/jira");

        // Invoke
        final String resourceUrl = AtlassianPluginsTestRunner.getResourceUrl(mockDescription);

        // Check
        assertThat(resourceUrl, is("http://www.example.com/jira/rest/atlassiantestrunner/1.0/runtest/MyTest"));
    }
}
