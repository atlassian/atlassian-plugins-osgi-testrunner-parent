package com.atlassian.plugins.osgi.test.rest;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * @since version
 */
public class TestResultDetailRepresentation extends TestDetailRepresentation
{

    @JsonProperty
    private Result testResult;
    @JsonProperty private Set<String> passedMethods;
    @JsonProperty private Set<String> ignoredMethods;
    @JsonProperty private Map<String,Failure> failedMethods;
    

    @JsonCreator
    public TestResultDetailRepresentation(@JsonProperty("classname") String classname, @JsonProperty("testResult") Result testResult)
    {
        super(classname);
        
        this.testResult = testResult;

        this.passedMethods = new HashSet<>();
        this.ignoredMethods = new HashSet<>();
        this.failedMethods = new HashMap<>();
    }

    public Result getTestResult()
    {
        return testResult;
    }

    public Set<String> getPassedMethods()
    {
        return passedMethods;
    }

    public Set<String> getIgnoredMethods()
    {
        return ignoredMethods;
    }

    public Map<String,Failure> getFailedMethods()
    {
        return failedMethods;
    }
}
