package com.atlassian.plugins.osgi.test.rest;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;

public class UpmOptionAdapter implements JsonSerializer<Optional>, JsonDeserializer<Optional> {
    private final Gson gson = new Gson();

    @Override
    public Optional deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (typeOfT instanceof ParameterizedType) {
            Type nestedType = ((ParameterizedType) typeOfT).getActualTypeArguments()[0];
            return Optional.of(gson.fromJson(json, nestedType));
        }

        return Optional.empty();
    }

    @Override
    public JsonElement serialize(Optional src, Type typeOfSrc, JsonSerializationContext context) {
        if (typeOfSrc instanceof ParameterizedType) {
            Type nestedType = ((ParameterizedType) typeOfSrc).getActualTypeArguments()[0];
            if (src.isPresent()) {
                return context.serialize(gson.toJson(src.get(), nestedType));
            }
        }
        return null;
    }
}
