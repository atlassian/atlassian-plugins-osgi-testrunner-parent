package com.atlassian.plugins.osgi.test.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.List;

/**
 * @since version
 */
public class TestDetailRepresentation
{
    @JsonProperty
    private final String classname;
    @JsonProperty private final String divId;
    @JsonProperty private List<String> allTestMethods;
    
    @JsonCreator
    public TestDetailRepresentation(@JsonProperty("classname") String classname)
    {
        this.classname = classname;
        this.divId = classname.replaceAll("\\.","-");
        this.allTestMethods = Collections.emptyList();
    }

    public String getClassname()
    {
        return classname;
    }

    public String getDivId()
    {
        return divId;
    }

    public List<String> getAllTestMethods()
    {
        return allTestMethods;
    }

    public void setAllTestMethods(List<String> allTestMethods)
    {
        this.allTestMethods = allTestMethods;
    }
}
