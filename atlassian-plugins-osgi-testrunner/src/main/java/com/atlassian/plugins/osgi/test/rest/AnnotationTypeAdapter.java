package com.atlassian.plugins.osgi.test.rest;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * @since version
 */
public class AnnotationTypeAdapter implements JsonSerializer<Annotation>
{

    @Override
    public JsonElement serialize(Annotation annotation, Type type, JsonSerializationContext jsonSerializationContext)
    {
        return JsonNull.INSTANCE;
    }
}
