# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

This file is known to be incomplete. Developers making further changes to this project should nevertheless add entries
for any changes that they make. 

## 2.0.6
### Changed
- Update Platform to 6.5.6, tested with Platform 7.0
- Update asm to 9.6 and ensure Java 21 compatibility
- Update hamcrest

## 2.0.2
### Added
- Added `README.md` (addressing [PTRUNNER-30](https://ecosystem.atlassian.net/browse/PTRUNNER-30)) and `CHANGELOG.md`.
- Added configuration files for the `jenv` and `mvmvm` development tools.
### Changed
- Tidied the Maven POMs using `mvn tidy:pom`
