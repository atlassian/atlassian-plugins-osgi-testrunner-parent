Purpose
=======
This project provides the framework for P2 plugin developers to write so-called
[Wired Tests](https://developer.atlassian.com/server/framework/atlassian-sdk/run-wired-tests-with-the-plugin-test-console/).

Structure
=========
This project contains several modules:

* `atlassian-plugins-osgi-testrunner`: a JAR file that provides P2 developers with the custom JUnit 4 test runner
(`AtlassianPluginsTestRunner`) used by this framework,
* `atlassian-plugins-osgi-testrunner-bundle`: a P2 plugin, typically loaded by AMPS, that invokes the in-product wired
tests, and
* `atlassian-osgi-testrunner-test-plugin`: a dummy P2 plugin with its own wired tests, for testing this framework
(although this module is possibly not used, as it's not in the reactor and its POM has problems like an undefined
version property and a reference to a former developer's home folder).

Disambiguation
==============
The framework that this project provides is not to be confused with that provided by the
[Func Test Plugin](https://bitbucket.org/atlassian/functest-plugin/), which works slightly differently but does much the
same thing (it provides a `<junit>` plugin module type that allows P2 developers to declare test suites in
`atlassian-plugin.xml`).

Ownership
=========
This project is currently unowned, however the Server Java Platform team is considering taking it on, as it provides the
framework used by AMPS' `remote-test` goal.

Running
=======
To run this project locally (just the Test Runner plugin):

1. From the root directory, `mvn clean install -DskipTests`
1. `cd atlassian-plugins-osgi-testrunner-bundle`
1. `mvn amps:run -DskipTests`
1. Point your browser to http://localhost:5990/refapp/plugins/servlet/upm/manage/all; the "TestRunner OSGi Plugin" and
its 13 or so modules should be enabled.
1. Point your browser to http://localhost:5990/refapp/plugins/servlet/it-test-console; the "Atlassian Plugin Test
Console" should appear, showing zero tests.

To run the test plugin locally:

1. From the root directory, `mvn clean install -DskipTests`
1. `cd atlassian-osgi-testrunner-test-plugin`
1. `mvn amps:run -DskipTests`
1. Point your browser to http://localhost:5990/refapp/plugins/servlet/upm/manage/all; the following plugins and all
their modules should be enabled:
    * The "TestRunner OSGi Plugin" (the plugin that provides the Wired Tests framework to the backend)
    * The "Test Plugin" (a plain P2 plugin with no modules; this is the plugin being tested by the Wired Tests)
    * The "Test Plugin Tests" (the Wired Tests for the "Test Plugin")
1. Point your browser to http://localhost:5990/refapp/plugins/servlet/it-test-console; the "Atlassian Plugin Test
Console" should appear, showing:
    * one "wired" test: `it.com.example.MyPluginTest` and
    * one "plain old" unit test: `com.example.CrapTest`
