package it.com.example;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Supplier;

import static java.lang.Boolean.getBoolean;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * A simple wired test that makes assertions about the product-provided {@link PluginAccessor}.
 */
@RunWith(AtlassianPluginsTestRunner.class)
public class MyPluginTest
{
    private static final String FAIL_ON_PURPOSE = MyPluginTest.class.getName() + ".fail";

    private static final Logger LOGGER = LoggerFactory.getLogger(MyPluginTest.class);

    private final PluginAccessor pluginAccessor;

    public MyPluginTest(PluginAccessor pluginAccessor)
    {
        this.pluginAccessor = pluginAccessor;
    }

    @Test
    public void testSomething()
    {
        LOGGER.info("I RAN!!!!!!!!!!!!!");
        assertNotNull(LOGGER);
    }

    @Test
    public void testSomeFailure()
    {
        if (getBoolean(FAIL_ON_PURPOSE)) {
            LOGGER.info("I RAN but failed...");
            fail("Deliberate test failure to exercise the test console");
        }
    }

    @Test
    public void testPluginAccessor()
    {
        assertNotNull(pluginAccessor);
        LOGGER.info("pluginAccessor = {}", pluginAccessor);
    }

    @Test
    public void testPluginAccessorWithHamcrest() {
        assertThat(pluginAccessor, is(notNullValue()));
    }

    @Test
    public void testPTRUNNER42()
    {
        final Supplier<String> s = this::toString;
        assertNotNull(s);
    }

    @Ignore("This is to test that OSGi Test Runner can identify tests to ignore")
    @Test
    public void ignoredTest()
    {
        fail("Should never be run");
    }
}
