package com.example;

import org.junit.Test;
import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * A non-wired JUnit test class.
 */
public class PlainOldUnitTest
{
    private static final Logger LOGGER = getLogger(PlainOldUnitTest.class);

    @Test
    public void testOne()
    {
        LOGGER.info("Test One.");
    }

    @Test
    public void testTwo()
    {
        LOGGER.info("Test Two.");
    }
}
